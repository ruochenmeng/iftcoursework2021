db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.Beta"} } }])
	
db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector": "Information Technology"} },
	{$group: {_id: "$StaticData.GICSSubIndustry", pratio: {$avg: "$FinancialRatios.PERatio"} } }])

db.CourseworkOne.find( { $and: [ { Symbol: { $in: ['AAPL','ACN', 'AMAT', 'AVGO', 'IBM', 'NVDA', 'ORCL'] }, "FinancialRatios.PERatio": {"$gte": 15} } ] } ).pretty()
