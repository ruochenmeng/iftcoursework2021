
--Query1
db.CourseworkOne.find({$and:[{"FinancialRatios.PERatio":{"$gte":25}},
                            {"MarketData.MarketCap":{"$gte":60000}},
                           {"StaticData.GICSSector":{"$eq":"Information Technology"}}]})
{ _id: ObjectId("61a00255c455b1c706240db7"),
  Symbol: 'ADBE',
  StaticData: 
   { Security: 'Adobe Systems Inc',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Application Software' },
  MarketData: { Price: 261.32, MarketCap: 126854, Beta: 1.54 },
  FinancialRatios: { DividendYield: 0, PERatio: 45.8, PayoutRatio: 0 } }
{ _id: ObjectId("61a00255c455b1c706240ded"),
  Symbol: 'ADP',
  StaticData: 
   { Security: 'Automatic Data Processing',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Internet Services & Infrastructure' },
  MarketData: { Price: 160.04, MarketCap: 69409, Beta: 1.05 },
  FinancialRatios: { DividendYield: 1.9, PERatio: 30.3, PayoutRatio: 58 } }
{ _id: ObjectId("61a00255c455b1c706240e03"),
  Symbol: 'AVGO',
  StaticData: 
   { Security: 'Broadcom',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Semiconductors' },
  MarketData: { Price: 280.32, MarketCap: 111194, Beta: 1.3 },
  FinancialRatios: { DividendYield: 3.5, PERatio: 37.8, PayoutRatio: 130.8 } }
{ _id: ObjectId("61a00255c455b1c706240e73"),
  Symbol: 'FIS',
  StaticData: 
   { Security: 'Fidelity National Information Services',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Internet Software & Services' },
  MarketData: { Price: 126.97, MarketCap: 77888, Beta: 0.89 },
  FinancialRatios: { DividendYield: 1.1, PERatio: 54.6, PayoutRatio: 57.7 } }
{ _id: ObjectId("61a00255c455b1c706240eb4"),
  Symbol: 'INTU',
  StaticData: 
   { Security: 'Intuit Inc.',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Internet Software & Services' },
  MarketData: { Price: 255.97, MarketCap: 66571, Beta: 1.29 },
  FinancialRatios: { DividendYield: 0.7, PERatio: 42.7, PayoutRatio: 31.4 } }
{ _id: ObjectId("61a00255c455b1c706240ee6"),
  Symbol: 'MA',
  StaticData: 
   { Security: 'Mastercard Inc.',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'IT Services' },
  MarketData: { Price: 260.85, MarketCap: 264647, Beta: 1.39 },
  FinancialRatios: { DividendYield: 0.5, PERatio: 40, PayoutRatio: 19 } }
{ _id: ObjectId("61a00255c455b1c706240ef3"),
  Symbol: 'MSFT',
  StaticData: 
   { Security: 'Microsoft Corp.',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Systems Software' },
  MarketData: { Price: 137.24, MarketCap: 1047883, Beta: 1.34 },
  FinancialRatios: { DividendYield: 1.3, PERatio: 26.9, PayoutRatio: 36 } }
{ _id: ObjectId("61a00255c455b1c706240f14"),
  Symbol: 'NVDA',
  StaticData: 
   { Security: 'Nvidia Corporation',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Semiconductors' },
  MarketData: { Price: 195.09, MarketCap: 118809, Beta: 2.07 },
  FinancialRatios: { DividendYield: 0.3, PERatio: 43.3, PayoutRatio: 14 } }
{ _id: ObjectId("61a00255c455b1c706240f1e"),
  Symbol: 'PYPL',
  StaticData: 
   { Security: 'PayPal',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Data Processing & Outsourced Services' },
  MarketData: { Price: 96.64, MarketCap: 113713, Beta: 1.41 },
  FinancialRatios: { DividendYield: 0, PERatio: 45.2, PayoutRatio: 0 } }
{ _id: ObjectId("61a00255c455b1c706240f38"),
  Symbol: 'QCOM',
  StaticData: 
   { Security: 'QUALCOMM Inc.',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Semiconductors' },
  MarketData: { Price: 77.24, MarketCap: 93897, Beta: 1 },
  FinancialRatios: { DividendYield: 3.2, PERatio: 27.1, PayoutRatio: 87 } }
{ _id: ObjectId("61a00255c455b1c706240f49"),
  Symbol: 'CRM',
  StaticData: 
   { Security: 'Salesforce.com',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'Internet Software & Services' },
  MarketData: { Price: 145.13, MarketCap: 113038, Beta: 1.52 },
  FinancialRatios: { DividendYield: 0, PERatio: 117.6, PayoutRatio: 0 } }
{ _id: ObjectId("61a00255c455b1c706240f8d"),
  Symbol: 'V',
  StaticData: 
   { Security: 'Visa Inc.',
     SECfilings: 'reports',
     GICSSector: 'Information Technology',
     GICSSubIndustry: 'IT Services' },
  MarketData: { Price: 171.32, MarketCap: 297822, Beta: 1.17 },
  FinancialRatios: { DividendYield: 0.6, PERatio: 32.9, PayoutRatio: 18.4 } }









--Query2
  db.CourseworkOne.aggregate([{$group : {_id:"$StaticData.GICSSector", 
                                      avg_PayoutRatio : {$avg : "$FinancialRatios.PayoutRatio"}, avg_PERatio :
                                      {$avg : "$FinancialRatios.PERatio"}}}])
{ _id: 'Communication Services',
  avg_PayoutRatio: 18.065384615384616,
  avg_PERatio: 24.430769230769233 }
{ _id: 'Utilities',
  avg_PayoutRatio: 62.17142857142857,
  avg_PERatio: 22.535714285714285 }
{ _id: 'Financials',
  avg_PayoutRatio: 11.36969696969697,
  avg_PERatio: 6.868181818181818 }
{ _id: 'Energy',
  avg_PayoutRatio: 59.675000000000004,
  avg_PERatio: 4.539285714285716 }
{ _id: 'Information Technology',
  avg_PayoutRatio: 21.18955223880597,
  avg_PERatio: 37.15223880597015 }
{ _id: 'Materials',
  avg_PayoutRatio: 14.228,
  avg_PERatio: 8.764000000000001 }
{ _id: 'Real Estate',
  avg_PayoutRatio: 1066.175,
  avg_PERatio: 250.9125 }
{ _id: 'Consumer Discretionary',
  avg_PayoutRatio: 32.39193548387097,
  avg_PERatio: 22.901612903225807 }
{ _id: 'Health Care',
  avg_PayoutRatio: 18.979032258064517,
  avg_PERatio: 26.246774193548386 }
{ _id: 'Consumer Staples',
  avg_PayoutRatio: 54.448484848484846,
  avg_PERatio: 24.7 }
{ _id: 'Industrials',
  avg_PayoutRatio: 30.325,
  avg_PERatio: 20.416176470588233 }