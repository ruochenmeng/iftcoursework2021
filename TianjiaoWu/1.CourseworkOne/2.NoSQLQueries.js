// Return all distinct sectors
db.CourseworkOne.distinct("StaticData.GICSSector")

// Find all equities with Payout ratio between 35 and 55
db.CourseworkOne.find({"FinancialRatios.PayoutRatio": {"$gte": 35, "$lte": 55 }}, {Symbol: 1, "StaticData.Security": 1, "StaticData.GICSSector": 1}).pretty()

// Count all equities with PE ratio between 20 and 25
db.CourseworkOne.find({"FinancialRatios.PERatio": {"$gte": 20, "$lte": 25 }}).count()

// Aggregate all Sectors and return the average Beta by Sector for only these stocks that have a market cap greater than or equal to 10000
db.CourseworkOne.aggregate([                                                                 {$match: {"MarketData.MarketCap": {"$gte": 10000} } },                               {$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.Beta"} } }])